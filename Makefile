# ---------------------------------------------------------------------------- #
# PROJECT DATA
# ---------------------------------------------------------------------------- #

NAME		=	lem-in

# ---------------------------------------------------------------------------- #

SRCS		=	main.c						\
				get_next_line.c				\
				ft_isstrdigit.c				\
				ft_error_gestion.c			\
				get_tokens.c				\
				ft_ts_new.c					\
				ft_ts_add.c					\
				ft_ts_add_btw.c				\
				ft_ts_add_end.c				\
				ft_rep_whitespaces.c 		\
				ft_replace.c 				\
				ft_rtrim.c 					\
				ft_ltrim.c 					\
				ft_trim.c 					\
				ft_is_whitespace.c 			\
				ft_lentabstr.c				\
				is_valid_def_room.c			\
				isvalid_links.c				\
				isvalid_ants_nbr.c			\
				isvalid_room_name.c 		\
				check_lst_tokens.c 			\
				ft_ts_pop_last.c 			\
				ft_ts_print.c 				\
				process_tokens_lst.c  		\
				ft_ts_remove_ret_next.c 	\
				ft_ts_pop_room.c 			\
				ft_rs_add_end.c 			\
				ft_rs_new.c 				\
				ft_rs_print.c 				\
				transform_and_launch.c		\
				ft_rs_add_link.c 			\
				ft_ls_print.c 				\
				ft_ls_new.c 				\
				roo_exist_update.c 			\
				ft_ol_new.c 				\
				launch_algo.c 				\
				ft_ol_add.c 				\
				ft_cl_new.c 				\
				ft_ol_pop.c 				\
				solution_path.c 			\
				get_links.c 				\
				ft_cl_add.c 				\
				ft_rs_copy.c 				\
				print_map_infos.c			\

# ---------------------------------------------------------------------------- #
# PROJECT CONFIGURATION
# ---------------------------------------------------------------------------- #

ROOT_DIR 	= `pwd`

CFLAGS		=	\
				-Wall -Wextra -Werror \

# >>> REQUIRED FOR LIBRARIES >>> THINK ABOUT CHANGING THE *LIBS rules

CPPFLAGS	=	\
				-I $(DIRINC) -I $(DIRINCBIS) 		\

LDFLAGS		=	\

LDLIBS		=	-L $(DIRLIB) -lft	 \

# GLOBAL SETUP
AR			=	ar
CC			=	clang
RM			=	rm
MKDIR		=	mkdir
MAKE		=	make

DIRSRC		=	./srcs/
DIROBJ		=	./.objs/
DIRINC		=	./incs/
DIRINCBIS	=	./libs/libft/
DIRLIB		=	./libs/libft/

# EXTRA COLOR AND DISPLAY FEATURE DE OUF
C_DFL		=	\033[0m
C_GRA		=	\033[30m
C_RED		=	\033[31m
C_GRE		=	\033[32m
C_YEL		=	\033[33m
C_BLUE		=	\033[34m
C_MAG		=	\033[35m
C_CYA		=	\033[36m
C_WHI		=	\033[37m

# ============================================================================ #

# ---------------------------------------------------------------------------- #
# SOURCES NORMALIZATION
# ---------------------------------------------------------------------------- #

SRC			=	$(addprefix $(DIRSRC), $(SRCS))
OBJ			=	$(addprefix $(DIROBJ), $(notdir $(SRC:.c=.o)))

# ---------------------------------------------------------------------------- #
# RULES
# ---------------------------------------------------------------------------- #

all			:	$(NAME)
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) build completed\n" "$(MAKE)"

$(NAME)		:	$(DIROBJ) $(OBJ) libs
	@make -C libs/libft/
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) linking objects\n" "$(CC)"
	@$(CC) $(OBJ) -o $(NAME) $(LDFLAGS) $(LDLIBS)

# ---------------------------------------------------------------------------- #
# CUSTOMISABLE RULES
# ---------------------------------------------------------------------------- #

clean		:
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) remove objects\n" "$(RM)"
	@$(RM) -rf $(DIROBJ)

fclean		:	clean
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) remove binaries\n" "$(RM)"
	@$(RM) -f $(NAME)

re			:	fclean all

$(DIROBJ)	:
	@$(MKDIR) -p $(DIROBJ)

depend		:
	@sed -e '/^#start/,/^end/d' Makefile > .mftmp && mv .mftmp Makefile
	@printf "#start\n\n" >> Makefile
	@$(foreach s, $(SRC),																\
		printf '$$(DIROBJ)'										>> Makefile	&& \
		$(CC) -MM $(s) $(CPPFLAGS)								>> Makefile	&& \
																			\
		printf '\t\t@printf "$$(C_GRE)[ $(NAME) ] '				>> Makefile && \
		printf '[ %%-8s ]$$(C_DFL) " "$(CC)"\n'					>> Makefile && \
		printf '\t\t@printf "compiling $(s)\\n"\n'				>> Makefile	&& \
																			\
		printf '\t\t@$$(CC) -c $(s) -o '						>> Makefile	&& \
		printf '$(addprefix $(DIROBJ), $(notdir $(s:.c=.o))) '>> Makefile	&& \
		printf '$$(CPPFLAGS) $$(CFLAGS)\n\n'					>> Makefile	&& \
																			\
		printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) " "dep"				&& \
		printf "$(s) rule generated\n"										|| \
																			\
		(sed -e '/^#start/,$$d' Makefile > .mftmp && mv .mftmp Makefile		&& \
		printf "#start\n\n"										>> Makefile	&& \
		printf "$(C_RED)[ $(NAME) ] [ %-8s ]$(C_DFL) " "dep"				&& \
		printf "$(s) rule generation failed\n"								) \
	;)
	@printf "\n#end\n" >> Makefile

.PHONY	:	 libs

# ---------------------------------------------------------------------------- #
# AUTO-GENERATED SECTION - do not modify
# ---------------------------------------------------------------------------- #

#start

$(DIROBJ)main.o: srcs/main.c incs/lemin.h incs/get_next_line.h libs/libft/libft.h \
  libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/main.c\n"
		@$(CC) -c ./srcs/main.c -o ./.objs/main.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)get_next_line.o: srcs/get_next_line.c incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/get_next_line.c\n"
		@$(CC) -c ./srcs/get_next_line.c -o ./.objs/get_next_line.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_isstrdigit.o: srcs/ft_isstrdigit.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_isstrdigit.c\n"
		@$(CC) -c ./srcs/ft_isstrdigit.c -o ./.objs/ft_isstrdigit.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_error_gestion.o: srcs/ft_error_gestion.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_error_gestion.c\n"
		@$(CC) -c ./srcs/ft_error_gestion.c -o ./.objs/ft_error_gestion.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)get_tokens.o: srcs/get_tokens.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/get_tokens.c\n"
		@$(CC) -c ./srcs/get_tokens.c -o ./.objs/get_tokens.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ts_new.o: srcs/ft_ts_new.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ts_new.c\n"
		@$(CC) -c ./srcs/ft_ts_new.c -o ./.objs/ft_ts_new.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ts_add.o: srcs/ft_ts_add.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ts_add.c\n"
		@$(CC) -c ./srcs/ft_ts_add.c -o ./.objs/ft_ts_add.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ts_add_btw.o: srcs/ft_ts_add_btw.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ts_add_btw.c\n"
		@$(CC) -c ./srcs/ft_ts_add_btw.c -o ./.objs/ft_ts_add_btw.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ts_add_end.o: srcs/ft_ts_add_end.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ts_add_end.c\n"
		@$(CC) -c ./srcs/ft_ts_add_end.c -o ./.objs/ft_ts_add_end.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_rep_whitespaces.o: srcs/ft_rep_whitespaces.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_rep_whitespaces.c\n"
		@$(CC) -c ./srcs/ft_rep_whitespaces.c -o ./.objs/ft_rep_whitespaces.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_replace.o: srcs/ft_replace.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_replace.c\n"
		@$(CC) -c ./srcs/ft_replace.c -o ./.objs/ft_replace.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_rtrim.o: srcs/ft_rtrim.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_rtrim.c\n"
		@$(CC) -c ./srcs/ft_rtrim.c -o ./.objs/ft_rtrim.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ltrim.o: srcs/ft_ltrim.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ltrim.c\n"
		@$(CC) -c ./srcs/ft_ltrim.c -o ./.objs/ft_ltrim.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_trim.o: srcs/ft_trim.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_trim.c\n"
		@$(CC) -c ./srcs/ft_trim.c -o ./.objs/ft_trim.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_is_whitespace.o: srcs/ft_is_whitespace.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_is_whitespace.c\n"
		@$(CC) -c ./srcs/ft_is_whitespace.c -o ./.objs/ft_is_whitespace.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_lentabstr.o: srcs/ft_lentabstr.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_lentabstr.c\n"
		@$(CC) -c ./srcs/ft_lentabstr.c -o ./.objs/ft_lentabstr.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)is_valid_def_room.o: srcs/is_valid_def_room.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/is_valid_def_room.c\n"
		@$(CC) -c ./srcs/is_valid_def_room.c -o ./.objs/is_valid_def_room.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)isvalid_links.o: srcs/isvalid_links.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/isvalid_links.c\n"
		@$(CC) -c ./srcs/isvalid_links.c -o ./.objs/isvalid_links.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)isvalid_ants_nbr.o: srcs/isvalid_ants_nbr.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/isvalid_ants_nbr.c\n"
		@$(CC) -c ./srcs/isvalid_ants_nbr.c -o ./.objs/isvalid_ants_nbr.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)isvalid_room_name.o: srcs/isvalid_room_name.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/isvalid_room_name.c\n"
		@$(CC) -c ./srcs/isvalid_room_name.c -o ./.objs/isvalid_room_name.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)check_lst_tokens.o: srcs/check_lst_tokens.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/check_lst_tokens.c\n"
		@$(CC) -c ./srcs/check_lst_tokens.c -o ./.objs/check_lst_tokens.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ts_pop_last.o: srcs/ft_ts_pop_last.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ts_pop_last.c\n"
		@$(CC) -c ./srcs/ft_ts_pop_last.c -o ./.objs/ft_ts_pop_last.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ts_print.o: srcs/ft_ts_print.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ts_print.c\n"
		@$(CC) -c ./srcs/ft_ts_print.c -o ./.objs/ft_ts_print.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)process_tokens_lst.o: srcs/process_tokens_lst.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/process_tokens_lst.c\n"
		@$(CC) -c ./srcs/process_tokens_lst.c -o ./.objs/process_tokens_lst.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ts_remove_ret_next.o: srcs/ft_ts_remove_ret_next.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ts_remove_ret_next.c\n"
		@$(CC) -c ./srcs/ft_ts_remove_ret_next.c -o ./.objs/ft_ts_remove_ret_next.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ts_pop_room.o: srcs/ft_ts_pop_room.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ts_pop_room.c\n"
		@$(CC) -c ./srcs/ft_ts_pop_room.c -o ./.objs/ft_ts_pop_room.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_rs_add_end.o: srcs/ft_rs_add_end.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_rs_add_end.c\n"
		@$(CC) -c ./srcs/ft_rs_add_end.c -o ./.objs/ft_rs_add_end.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_rs_new.o: srcs/ft_rs_new.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_rs_new.c\n"
		@$(CC) -c ./srcs/ft_rs_new.c -o ./.objs/ft_rs_new.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_rs_print.o: srcs/ft_rs_print.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_rs_print.c\n"
		@$(CC) -c ./srcs/ft_rs_print.c -o ./.objs/ft_rs_print.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)transform_and_launch.o: srcs/transform_and_launch.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/transform_and_launch.c\n"
		@$(CC) -c ./srcs/transform_and_launch.c -o ./.objs/transform_and_launch.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_rs_add_link.o: srcs/ft_rs_add_link.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_rs_add_link.c\n"
		@$(CC) -c ./srcs/ft_rs_add_link.c -o ./.objs/ft_rs_add_link.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ls_print.o: srcs/ft_ls_print.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ls_print.c\n"
		@$(CC) -c ./srcs/ft_ls_print.c -o ./.objs/ft_ls_print.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ls_new.o: srcs/ft_ls_new.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ls_new.c\n"
		@$(CC) -c ./srcs/ft_ls_new.c -o ./.objs/ft_ls_new.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)roo_exist_update.o: srcs/roo_exist_update.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/roo_exist_update.c\n"
		@$(CC) -c ./srcs/roo_exist_update.c -o ./.objs/roo_exist_update.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ol_new.o: srcs/ft_ol_new.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ol_new.c\n"
		@$(CC) -c ./srcs/ft_ol_new.c -o ./.objs/ft_ol_new.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)launch_algo.o: srcs/launch_algo.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/launch_algo.c\n"
		@$(CC) -c ./srcs/launch_algo.c -o ./.objs/launch_algo.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ol_add.o: srcs/ft_ol_add.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ol_add.c\n"
		@$(CC) -c ./srcs/ft_ol_add.c -o ./.objs/ft_ol_add.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_cl_new.o: srcs/ft_cl_new.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_cl_new.c\n"
		@$(CC) -c ./srcs/ft_cl_new.c -o ./.objs/ft_cl_new.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_ol_pop.o: srcs/ft_ol_pop.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_ol_pop.c\n"
		@$(CC) -c ./srcs/ft_ol_pop.c -o ./.objs/ft_ol_pop.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)solution_path.o: srcs/solution_path.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/solution_path.c\n"
		@$(CC) -c ./srcs/solution_path.c -o ./.objs/solution_path.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)get_links.o: srcs/get_links.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/get_links.c\n"
		@$(CC) -c ./srcs/get_links.c -o ./.objs/get_links.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_cl_add.o: srcs/ft_cl_add.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_cl_add.c\n"
		@$(CC) -c ./srcs/ft_cl_add.c -o ./.objs/ft_cl_add.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)ft_rs_copy.o: srcs/ft_rs_copy.c incs/lemin.h incs/get_next_line.h \
  libs/libft/libft.h libs/libft/structs.h incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/ft_rs_copy.c\n"
		@$(CC) -c ./srcs/ft_rs_copy.c -o ./.objs/ft_rs_copy.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)print_map_infos.o: srcs/print_map_infos.c incs/lemin.h \
  incs/get_next_line.h libs/libft/libft.h libs/libft/structs.h \
  incs/lemin_structs.h
		@printf "$(C_GRE)[ lem-in ] [ %-8s ]$(C_DFL) " "clang"
		@printf "compiling ./srcs/print_map_infos.c\n"
		@$(CC) -c ./srcs/print_map_infos.c -o ./.objs/print_map_infos.o $(CPPFLAGS) $(CFLAGS)


#end
