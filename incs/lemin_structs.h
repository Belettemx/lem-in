/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 17:28:32 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_STRUCTS_H
# define LEMIN_STRUCTS_H

/*
** Skip stand for commentaries, cmd without meaning.
** invalid is for invalid lines
*/

typedef enum		e_type
{
	t_cmd_start,
	t_cmd_end,
	t_def_room,
	t_link,
	t_skip,
	t_invalid,
	t_ants,
	t_tmp
}					t_type;

typedef enum		e_status
{
	dead,
	norm
}					t_status;

typedef struct			s_tokens
{
	t_type				type;
	char				*str;
	char				*x;
	char				*y;
	struct s_tokens		*next;
	struct s_tokens		*prev;

}						t_tok;

typedef struct			s_room
{
	char				*name;
	char				*x;
	char				*y;
	int					cost;
	int					isempty;
	int					ant;
	int					lock;
	t_type				type;
	t_status			stat;
	struct s_room		*next;
	struct s_room		*prev;
	struct s_link		*links;
	struct s_room		*parent;
}						t_room;

typedef struct			s_env
{
	int					ants_nbr;
	struct s_room		*first;
	struct s_room		*last;
	struct s_room		*start;
	struct s_room		*end;
	struct s_room		*tmp;
	struct s_path		*paths;
}						t_env;

typedef struct			s_link
{
	struct s_room		*link;
	struct s_link		*next;
}						t_links;

typedef struct			s_open_lst
{
	int					cost;
	struct s_room		*elem;
	struct s_open_lst	*next;
}						t_open_lst;

typedef struct			s_cls_lst
{
	struct s_room		*elem;
	struct s_cls_lst	*next;
}						t_cls_lst;

typedef struct			s_path
{
	struct s_room		*path;
	struct s_path		*next;
}						t_path;

#endif
