/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 17:28:32 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# include "get_next_line.h"
# include "libft.h"
# include "lemin_structs.h"

# define TRUE 1
# define FALSE 0
# define ERROR -1

int			ft_get_ants_nbr(char *str);
int			ft_isstrdigit(char *str);
void		ft_error_gestion(char *s);
t_tok		*ft_ts_new(t_type type, char *str, char *x, char *y);
void		ft_ts_add_end(t_tok **first, t_tok **last, t_tok *new_elem);
void		ft_ts_add_btw(t_tok **before, t_tok **after, t_tok *new);
void		ft_ts_add(t_tok **first, t_tok **last, t_tok *new);
t_tok		*get_tokens(char *str);
int			ft_replace(char *str, char c, char r);
int			ft_rep_whitespaces(char *str, char r);
void		ft_rtrim(char *str);
void		ft_ltrim(char *str);
void		ft_trim(char *str);
int			ft_is_whitespace(char c);
int			ft_lentabstr(char **tab);
int			is_valid_def_room(char **tab);
int			isvalid_links(char **tab);
int			isvalid_ants_nbr(char *str);
int			isvalid_room_name(char *str);
int			check_lst_tokens(t_tok **first);
t_tok		*ft_ts_pop_last(t_tok **first, t_tok **last);
void		ft_ts_print(t_tok **first);
int			process_tokens_lst(t_tok **first, t_tok **last);
t_tok		*ft_ts_remove_ret_next(t_tok *elem);
t_tok		*ft_ts_pop_room(t_tok *elem);
void		ft_rs_print(t_room **first);
t_room		*ft_rs_new(t_type type, char *name, char *x, char *y);
void		transform_and_launch(t_tok **first, t_env *env);
void		ft_rs_add_end(t_room **first, t_room **last, t_room *new_elem);
void		ft_rs_add_link(t_room **first, t_tok *cur);
void		ft_ls_print(t_room *room);
void		ft_ls_new(t_room **room1, t_room **room2);
int			roo_exist_update(t_tok **first, t_tok *elem);
int			launch_algo(t_env *env, t_open_lst **op_first, t_cls_lst **cl_firs);
t_open_lst	*ft_ol_new(t_room *src, t_room *parent);
void		ft_ol_add(t_open_lst **op_first, t_open_lst *elem);
t_room		*ft_ol_pop(t_open_lst **first);
t_room		*get_links(t_room *elem);
int			solution_path(t_cls_lst **first, t_room *elem, t_env *env);
void		ft_cl_add(t_cls_lst **first, t_cls_lst *elem);
t_cls_lst	*ft_cl_new(t_room *elem);
void		ft_print_ol(t_open_lst **op_first);
void		ft_print_cl(t_cls_lst **op_first);
t_room		*ft_rs_copy(t_room *src);
void		print_map_infos(t_tok **first);

#endif
