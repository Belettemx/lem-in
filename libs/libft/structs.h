/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 19:29:21 by agauci-d          #+#    #+#             */
/*   Updated: 2014/12/30 15:00:13 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_H
# define STRUCTS_H

# include <string.h>

typedef struct			s_list
{
	void				*content;
	size_t				content_size;
	struct s_list		*next;
}						t_list;

typedef struct			s_listdbl
{
	void				*content;
	size_t				content_size;
	struct s_listdbl	*next;
	struct s_listdbl	*prev;
}						t_listdbl;

typedef struct			s_listaz
{
	struct s_listdbl	**alist;
	struct s_listdbl	**zlist;
}						t_listaz;

#endif
