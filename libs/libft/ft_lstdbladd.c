/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdbladd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2015/01/04 15:49:25 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Description
** Pour une liste doublement chainee.
** Ajoute l element new en tete de la liste.
** Param. #1
** L adresse d un pointeur sur le premier maillon d une liste.
** Param. #2
** Le maillon a ajouter en tete de cette liste.
*/

void	ft_lstdbladd(t_listdbl **alst, t_listdbl **zlst, t_listdbl *new)
{
	if (alst && !*alst && zlst && !*zlst)
	{
		*alst = new;
		*zlst = new;
	}
	else if (alst && *alst && new)
	{
		new->next = *alst;
		new->prev = NULL;
		(*alst)->prev = new;
		*alst = new;
	}
}
