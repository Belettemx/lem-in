/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/25 17:25:31 by agauci-d          #+#    #+#             */
/*   Updated: 2014/11/28 16:40:39 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** Descripition
** Prend en paramètre l’adresse d’un pointeur sur un maillon et libère
** la mémoire du contenu de ce maillon avec la fonction del passée en
** paramètre puis libère la mémoire du maillon en lui même avec free(3).
** La mémoire du champ next ne doit en aucun cas être libérée.
** Pour terminer, le pointeur sur le maillon maintenant libéré doit être
** mis à NULL (de manière similaire à la fonction ft_memdel de la partie
** obligatoire).
** Param. #1
** L’adresse d’un pointeur sur le maillon à libérer.
** Fonctions libc
** free(3)
*/

void	ft_lstdelone(t_list **alst, void (*del)(void *, size_t))
{
	t_list		*tmp;

	if (alst && *alst && del)
	{
		tmp = *alst;
		if (tmp->content != NULL)
			del(tmp->content, tmp->content_size);
		if (tmp != NULL)
			free(tmp);
		*alst = NULL;
	}
}
