/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdbladdend.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:48:33 by agauci-d          #+#    #+#             */
/*   Updated: 2014/11/27 11:02:58 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Pour une liste doubement chainee.
** Ajoute l'element new_elem en fin de liste.
*/

void	ft_lstdbladdend(t_listdbl **alst, t_listdbl **zlst, t_listdbl *new_elem)
{
	t_listdbl	*cpy;

	if (alst && *alst)
	{
		ft_putstr("error ft_lstdbladdend");
		return ;
	}
	if (alst && *alst && new_elem)
	{
		cpy = *alst;
		while (cpy->next != NULL)
			cpy = cpy->next;
		new_elem->prev = cpy;
		cpy->next = new_elem;
		*zlst = new_elem;
	}
}
