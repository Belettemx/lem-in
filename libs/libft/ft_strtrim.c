/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 18:42:35 by agauci-d          #+#    #+#             */
/*   Updated: 2014/11/28 13:12:06 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** Alloue (avec malloc(3)) et retourne une copie de la chaine passée en
** paramètre sans les espaces blancs au debut et à la fin de cette chaine.
** On considère comme espaces blancs les caractères ’ ’, ’\n’ et ’\t’. Si s
** ne contient pas d’espaces blancs au début ou à la fin, la fonction renvoie
** une copie de s. Si l’allocation echoue, la fonction renvoie NULL.
*/

char	*ft_strtrim(char const *s)
{
	unsigned int	i;
	size_t			j;
	int				k;
	char			*dst;

	i = 0;
	k = 0;
	j = ft_strlen(s) - 1;
	if (!s)
		return (NULL);
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && s[i] != '\0')
		i++;
	if (s[i] == '\0')
		return (dst = ft_strdup(""));
	while ((s[j] == ' ' || s[j] == '\n' || s[j] == '\t') && j > i)
		j--;
	dst = (char *)malloc(sizeof(char) * (i + (j - i) + 2));
	if (!dst)
		return (NULL);
	while (i < j + 1)
		dst[k++] = s[i++];
	dst[k] = '\0';
	return (dst);
}
