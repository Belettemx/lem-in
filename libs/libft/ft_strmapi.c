/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 11:26:34 by agauci-d          #+#    #+#             */
/*   Updated: 2014/11/11 15:15:40 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** Applique la fonction f à chaque caractère de la chaine de caractères
** passée en paramètre en précisant son index pour créer une nouvelle
** chaine “fraiche” (avec malloc(3)) résultant des applications
** successives de f.
*/

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	size_t			i;
	unsigned int	index;
	size_t			len;
	char			*dst;

	i = 0;
	index = 0;
	len = ft_strlen(s);
	dst = (char *)malloc(sizeof(char) * (len + 1));
	if (!s || !f)
		return (NULL);
	while (i < len)
	{
		dst[i] = f(index, s[i]);
		i++;
		index++;
	}
	dst[i] = '\0';
	return (dst);
}
