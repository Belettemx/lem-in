/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rs_print.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 10:39:39 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_rs_print(t_room **first)
{
	t_room *elem;

	if (first && *first)
	{
		elem = *first;
		while (elem)
		{
			ft_putstr("type:");
			ft_putnbr(elem->type);
			ft_putstr(", name:");
			ft_putstr(elem->name);
			ft_putstr(", x:");
			ft_putstr(elem->x);
			ft_putstr(", y:");
			ft_putstr(elem->y);
			ft_putstr(", cost:");
			ft_putnbr(elem->cost);
			ft_putstr(", isempty:");
			ft_putnbr(elem->isempty);
			ft_putstr(", ant:");
			ft_putnbr(elem->ant);
			elem = elem->next;
		}
	}
}
