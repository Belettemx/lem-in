/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ts_add_btw.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/13 19:15:39 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/02 17:09:28 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_ts_add_btw(t_tok **before, t_tok **after, t_tok *new)
{
	t_tok *tmp;

	if (!before && !*before && !after && !*after)
	{
		ft_error_gestion("Error: ft_ts_add_btw()");
		return ;
	}
	else
	{
		tmp = *before;
		new->prev = *before;
		new->next = *after;
		tmp->next = new;
		tmp = *after;
		tmp->prev = new;
	}
}
