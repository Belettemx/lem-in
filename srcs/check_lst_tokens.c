/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_lst_tokens.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 17:21:09 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int				room_exist(t_tok **first, t_tok *cur)
{
	t_tok		*t;
	int			room1;
	int			room2;

	t = *first;
	room1 = FALSE;
	room2 = FALSE;
	while (t)
	{
		if (t->type == t_def_room)
		{
			if (ft_strequ(cur->x, t->str))
				room1 = TRUE;
			if (ft_strequ(cur->y, t->str))
				room2 = TRUE;
		}
		t = t->next;
	}
	return (room1 && room2);
}

int				is_start_end_rooms(t_tok **first, t_tok *cur)
{
	int			start;
	int			end;
	t_tok		*tmp;

	start = FALSE;
	end = FALSE;
	if (first && *first)
	{
		tmp = *first;
		while (tmp != cur)
		{
			if (tmp->type == t_cmd_end)
				start = TRUE;
			if (tmp->type == t_cmd_start)
				end = TRUE;
			tmp = tmp->next;
		}
		if (tmp->prev->type == t_cmd_start || tmp->prev->type == t_cmd_end)
			return (FALSE);
	}
	return (start && end);
}

int				check_lst_tokens(t_tok **first)
{
	t_tok		*t;
	int			room;

	room = FALSE;
	t = *first;
	while (t)
	{
		if (t == *first && t->type != t_ants)
			return (ERROR);
		if (t != *first && t->type == t_ants)
			return (FALSE);
		if (t->type == t_def_room)
		{
			if (t->prev->type == t_link)
				return (FALSE);
			room = TRUE;
		}
		if (t->type == t_link)
		{
			if (!room || !is_start_end_rooms(first, t) || !room_exist(first, t))
				return (ERROR);
		}
		t = t->next;
	}
	return (TRUE);
}
