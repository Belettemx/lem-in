/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ts_pop_room.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/06 16:01:19 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

/*
** pop_room, retourne le premier element de la list de token qui est a pour type
** t_def_room. La recherche commence à partir de l'element donné en parramètre.
*/

t_tok	*ft_ts_pop_room(t_tok *elem)
{
	while (elem)
	{
		if (elem->type == t_def_room)
			return (elem);
		elem = elem->next;
	}
	return (NULL);
}
