/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ts_add_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 21:36:41 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 16:45:05 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_ts_add_end(t_tok **first, t_tok **last, t_tok *new_elem)
{
	t_tok	*cpy;

	if (!first && !*first)
	{
		ft_putstr("error ft_ts_add_end()");
		return ;
	}
	if (*first == NULL)
	{
		*first = new_elem;
		*last = new_elem;
	}
	else if (first && *first && new_elem)
	{
		cpy = *first;
		while (cpy->next != NULL)
			cpy = cpy->next;
		new_elem->prev = cpy;
		cpy->next = new_elem;
		*last = new_elem;
	}
}
