/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_map_infos.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/12 15:18:33 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	print_room_infos(t_tok *elem)
{
	ft_putstr(elem->str);
	ft_putstr(" ");
	ft_putstr(elem->x);
	ft_putstr(" ");
	ft_putendl(elem->y);
}

void	print_map_infos(t_tok **first)
{
	t_tok		*t;

	t = *first;
	ft_putnbr(ft_atoi(t->str));
	ft_putchar('\n');
	while (t)
	{
		if (t->type == t_def_room)
		{
			if (t && t->prev && t->prev->type == t_cmd_start)
				ft_putendl("##start");
			if (t && t->prev && t->prev->type == t_cmd_end)
				ft_putendl("##end");
			print_room_infos(t);
		}
		if (t->type == t_link)
			ft_putendl(t->str);
		t = t->next;
	}
	ft_putchar('\n');
}
