/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_links.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/15 16:29:02 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_room	*get_links(t_room *elem)
{
	static t_links	*cur = NULL;
	t_room			*tmp;

	if (!cur)
	{
		cur = elem->links;
	}
	else
		cur = cur->next;
	if (cur)
	{
		tmp = cur->link;
		return (ft_rs_copy(tmp));
	}
	return (NULL);
}
