/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ts_print.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/06 15:02:50 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_ts_print(t_tok **first)
{
	t_tok *elem;

	if (first && *first)
	{
		elem = *first;
		while (elem)
		{
			ft_putstr("type:");
			ft_putnbr(elem->type);
			ft_putstr("str:");
			ft_putstr(elem->str);
			elem = elem->next;
		}
	}
}
