/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ltrim.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 17:49:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/01 17:11:50 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_ltrim(char *str)
{
	int i;
	int len;

	i = 0;
	len = ft_strlen(str);
	while (str && str[i] && ft_is_whitespace(str[i]))
	{
		i++;
		len--;
	}
	str = ft_strsub(str, (unsigned int)i, (size_t)len);
}
