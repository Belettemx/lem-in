/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rs_add_link.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 16:34:33 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	found_rooms(t_room **first, t_room **room1, t_room **room2, t_tok *cur)
{
	t_room *t;

	t = *first;
	*room1 = NULL;
	*room2 = NULL;
	while (t)
	{
		if (t->type == t_def_room || t->type == t_cmd_start\
			|| t->type == t_cmd_end)
		{
			if (ft_strequ(cur->x, t->name))
				*room1 = t;
			if (ft_strequ(cur->y, t->name))
				*room2 = t;
		}
		if (*room1 && *room2)
			break ;
		t = t->next;
	}
}

void	ft_rs_add_link(t_room **first, t_tok *cur)
{
	t_room *room1;
	t_room *room2;

	found_rooms(first, &room1, &room2, cur);
	if (room1 && room2)
		ft_ls_new(&room1, &room2);
	else
		ft_error_gestion("ERROR\n");
}
