/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rs_add_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/09 21:36:41 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/07 09:29:49 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_rs_add_end(t_room **first, t_room **last, t_room *new_elem)
{
	t_room	*cpy;

	if (!first && !*first)
	{
		ft_error_gestion("error ft_ts_add_end()");
		return ;
	}
	if (*first == NULL)
	{
		*first = new_elem;
		*last = new_elem;
	}
	else if (first && *first && new_elem)
	{
		cpy = *first;
		while (cpy->next != NULL)
			cpy = cpy->next;
		new_elem->prev = cpy;
		cpy->next = new_elem;
		*last = new_elem;
	}
}
