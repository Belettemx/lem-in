/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ts_remove_ret_next.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/06 14:42:18 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lemin.h"

/*
** remove an element of token list, and return the elem next the one removed.
** Warning! This element can not be the first or the last one.
** In such case, behaviour of the function is underterminated.
*/

t_tok	*ft_ts_remove_ret_next(t_tok *elem)
{
	t_tok	*prev;
	t_tok	*next;

	if (elem)
	{
		next = elem->next;
		prev = elem->prev;
		prev->next = next;
		next->prev = prev;
		free(elem);
		return (next);
	}
	return (NULL);
}
