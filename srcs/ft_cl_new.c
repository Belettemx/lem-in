/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cl_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/13 16:03:41 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdlib.h>

t_cls_lst		*ft_cl_new(t_room *elem)
{
	t_cls_lst	*tmp;

	tmp = (t_cls_lst *)malloc(sizeof(t_cls_lst));
	if (tmp)
	{
		tmp->elem = elem;
		tmp->next = NULL;
	}
	else
		ft_error_gestion("ERROR\n");
	return (tmp);
}
