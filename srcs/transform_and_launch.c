/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transform_and_launch.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/12 15:18:33 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	transform_and_launch(t_tok **first, t_env *env)
{
	t_tok	*t;
	t_room	*elem;

	t = *first;
	env->ants_nbr = ft_atoi(t->str);
	env->first = NULL;
	env->last = NULL;
	env->paths = NULL;
	while (t)
	{
		if (t->type == t_def_room)
		{
			elem = ft_rs_new(t->type, t->str, t->x, t->y);
			if (!elem)
				ft_error_gestion("ERROR\n");
			if (t && t->prev && t->prev->type == t_cmd_start)
				elem->type = t_cmd_start;
			if (t && t->prev && t->prev->type == t_cmd_end)
				elem->type = t_cmd_end;
			ft_rs_add_end(&env->first, &env->last, elem);
		}
		if (t->type == t_link)
			ft_rs_add_link(&env->first, t);
		t = t->next;
	}
}
