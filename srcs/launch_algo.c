/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   launch_algo.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/15 17:09:31 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		is_in_cls_lst(t_cls_lst **cl_first, t_room *son)
{
	t_cls_lst	*tmp;
	t_room		*elem;

	if (cl_first && *cl_first)
	{
		tmp = *cl_first;
		while (tmp)
		{
			elem = tmp->elem;
			if (ft_strequ(elem->name, son->name) && ft_strequ(elem->x, son->x)\
				&& ft_strequ(elem->y, son->y))
				return (tmp->elem->cost <= son->cost);
			tmp = tmp->next;
		}
	}
	return (FALSE);
}

int		is_in_op_lst(t_open_lst **op_first, t_room *son)
{
	t_open_lst	*tmp;
	t_room		*elem;

	if (op_first && *op_first)
	{
		tmp = *op_first;
		while (tmp)
		{
			elem = tmp->elem;
			if (ft_strequ(elem->name, son->name) && ft_strequ(elem->x, son->x)\
				&& ft_strequ(elem->y, son->y))
				return (tmp->elem->cost <= son->cost);
			tmp = tmp->next;
		}
	}
	return (FALSE);
}

int		is_solution(t_room *elem, t_room *start)
{
	if (elem && start)
	{
		if (ft_strequ(elem->name, start->name) && ft_strequ(elem->x, start->x)\
			&& ft_strequ(elem->y, start->y))
			return (TRUE);
	}
	return (FALSE);
}

int		launch_algo(t_env *env, t_open_lst **op_first, t_cls_lst **cl_first)
{
	t_room	*elem;
	t_room	*son;
	int		i;

	elem = env->end;
	elem->cost = 0;
	ft_ol_add(op_first, ft_ol_new(elem, NULL));
	while (op_first && *op_first)
	{
		i = 0;
		elem = ft_ol_pop(op_first);
		if (is_solution(elem, env->start))
			return (solution_path(cl_first, elem, env));
		while ((son = get_links(elem)))
		{
			son->cost = elem->cost + 1;
			if (!is_in_cls_lst(cl_first, son) && !is_in_op_lst(op_first, son))
				ft_ol_add(op_first, ft_ol_new(son, elem));
			i++;
		}
		ft_cl_add(cl_first, ft_cl_new(elem));
	}
	ft_putendl("No path found.");
	return (ERROR);
}
