/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ts_add.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/02 17:09:21 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_ts_add(t_tok **first, t_tok **last, t_tok *new)
{
	if (!first && !*first && !last && !*last)
	{
		ft_error_gestion("Error : ft_ts_add()");
		return ;
	}
	else if (*first == NULL)
	{
		*first = new;
		*last = new;
	}
	else if (first && *first && new)
	{
		new->next = *first;
		new->prev = NULL;
		(*first)->prev = new;
		*first = new;
	}
}
