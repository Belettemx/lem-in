/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_tokens.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/05 14:21:47 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int				is_commentary(char *str)
{
	if (str && str[0] == '#')
		return (TRUE);
	return (FALSE);
}

t_type			which_command(char *str)
{
	if (ft_strequ(str, "##start"))
		return (t_cmd_start);
	else if (ft_strequ(str, "##end"))
		return (t_cmd_end);
	return (t_skip);
}

int				is_command(char *str)
{
	if (str && ft_strstr(str, "##"))
		return (TRUE);
	return (FALSE);
}

t_type			get_type(char *str)
{
	int			i;
	t_type		type;

	i = 0;
	type = t_invalid;
	if (str && str[i])
	{
		ft_trim(str);
		ft_rep_whitespaces(str, ' ');
		if (is_command(str))
			type = which_command(str);
		else if (is_commentary(str))
			type = t_skip;
		else
			type = t_tmp;
	}
	return (type);
}

t_tok			*get_tokens(char *str)
{
	t_type		type;
	char		**tab;

	type = get_type(str);
	if (type == t_tmp && (tab = ft_strsplit(str, ' ')))
	{
		if (ft_lentabstr(tab) == 3)
		{
			if (is_valid_def_room(tab))
				return (ft_ts_new(t_def_room, tab[0], tab[1], tab[2]));
		}
		else if (ft_lentabstr(tab) == 1)
		{
			if (isvalid_links(ft_strsplit(str, '-')))
			{
				tab = ft_strsplit(str, '-');
				return (ft_ts_new(t_link, str, tab[0], tab[1]));
			}
			else if (isvalid_ants_nbr(str))
				return (ft_ts_new(t_ants, str, 0, 0));
		}
		type = t_invalid;
	}
	return (ft_ts_new(type, str, 0, 0));
}
