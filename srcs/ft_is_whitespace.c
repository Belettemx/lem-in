/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_whitespace.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 17:49:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/02 17:08:09 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

/*
** return true if character 'c' given is one of the following characters
** ' '      space
** '\t'     horizontal tab
** '\n'     newline
** '\v'     vertical tab
** '\f'     feed
** '\r' 	carrige return
** return false otherwise
*/

int	ft_is_whitespace(char c)
{
	if (c == ' ')
		return (TRUE);
	else if (c == '\t')
		return (TRUE);
	else if (c == '\n')
		return (TRUE);
	else if (c == '\v')
		return (TRUE);
	else if (c == '\f')
		return (TRUE);
	else if (c == '\r')
		return (TRUE);
	return (FALSE);
}
