/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rs_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/15 13:11:02 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdlib.h>

t_room	*ft_rs_new(t_type type, char *name, char *x, char *y)
{
	t_room	*elem;

	elem = (t_room *)malloc(sizeof(t_room));
	if (elem)
	{
		elem->type = type;
		elem->name = name;
		elem->x = x;
		elem->y = y;
		elem->cost = -1;
		elem->isempty = TRUE;
		elem->ant = 0;
		elem->lock = FALSE;
		elem->stat = dead;
		elem->next = NULL;
		elem->prev = NULL;
		elem->links = NULL;
		elem->parent = NULL;
	}
	return (elem);
}
