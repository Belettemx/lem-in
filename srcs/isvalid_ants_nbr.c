/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isvalid_ants_nbr.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/02 17:10:57 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int				isvalid_ants_nbr(char *str)
{
	int res;

	res = FALSE;
	if (ft_isstrdigit(str))
	{
		res = ft_atoi(str);
		if (res <= 0)
			return (FALSE);
		res = TRUE;
	}
	return (res);
}
