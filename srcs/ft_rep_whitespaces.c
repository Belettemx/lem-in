/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rep_whitespaces.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/01 17:03:13 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

/*
** replace every white spaces of a string by a the character 'r' given
** ' '      space
** '\t'     horizontal tab
** '\n'     newline
** '\v'     vertical tab
** '\f'     feed
** '\r' 	carrige return
*/

int		ft_rep_whitespaces(char *str, char r)
{
	char			*ws;
	int				count;
	int				i;

	count = 0;
	i = 0;
	ws = " \t\n\v\f\r\0";
	while (ws && ws[i])
	{
		count += ft_replace(str, ws[i], r);
		i++;
	}
	return (count);
}
