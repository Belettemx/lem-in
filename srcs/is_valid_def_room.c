/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_valid_def_room.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/02 17:10:29 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int				isvalid_coord(char *str)
{
	if (ft_atoi(str) != 0)
		return (TRUE);
	if (ft_strlen(str) == 1 && str[0] == '0')
		return (TRUE);
	return (FALSE);
}

int				is_valid_def_room(char **tab)
{
	if (isvalid_room_name(tab[0]))
	{
		if (isvalid_coord(tab[1]) && isvalid_coord(tab[2]))
			return (TRUE);
	}
	return (FALSE);
}
