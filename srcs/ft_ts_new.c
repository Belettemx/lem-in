/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ts_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/13 13:57:03 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdlib.h>

t_tok	*ft_ts_new(t_type type, char *str, char *x, char *y)
{
	t_tok	*elem;

	elem = (t_tok *)malloc(sizeof(t_tok));
	if (elem)
	{
		elem->type = type;
		elem->str = ft_strdup(str);
		elem->x = x;
		elem->y = y;
		elem->next = NULL;
		elem->prev = NULL;
	}
	return (elem);
}
