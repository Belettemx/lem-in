/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_tokens_lst.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/12 16:34:52 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		is_start_end_diff_rooms(t_tok **last)
{
	t_tok	*end_room;
	t_tok	*start_room;
	t_tok	*t;

	end_room = NULL;
	start_room = NULL;
	if (last && *last)
	{
		t = *last;
		while (t)
		{
			if (t->type == t_cmd_start)
				start_room = ft_ts_pop_room(t->next);
			if (t->type == t_cmd_end)
				end_room = ft_ts_pop_room(t->next);
			if (start_room && end_room)
				return (start_room != end_room ? TRUE : FALSE);
			t = t->prev;
		}
	}
	return (FALSE);
}

int		check_start_end_room(t_tok **last)
{
	t_tok		*t;
	int			end;
	int			start;

	t = *last;
	end = FALSE;
	start = FALSE;
	while (t)
	{
		if (t->type == t_cmd_start)
		{
			if (start)
				t = ft_ts_remove_ret_next(t);
			start = TRUE;
		}
		if (t && t->type == t_cmd_end)
		{
			if (end)
				t = ft_ts_remove_ret_next(t);
			end = TRUE;
		}
		if (t)
			t = t->prev;
	}
	return (start && end && is_start_end_diff_rooms(last));
}

int		check_lst_before_process(t_tok **first)
{
	t_tok		*t;
	int			room;
	int			link;
	int			ants;

	room = FALSE;
	link = FALSE;
	ants = FALSE;
	t = *first;
	while (t)
	{
		if (t == *first && t->type == t_ants)
			ants = TRUE;
		if (t->type == t_def_room)
			room = TRUE;
		if (t->type == t_link)
		{
			if (!room)
				break ;
			link = TRUE;
		}
		t = t->next;
	}
	return (room && link && ants);
}

int		check_links_exists_start_and_rooms(t_env *env)
{
	t_room *tmp;

	env->start = NULL;
	env->end = NULL;
	if (env->first)
	{
		tmp = env->first;
		while (tmp)
		{
			if (tmp->type == t_cmd_end)
			{
				if (tmp->links)
					env->end = tmp;
			}
			if (tmp->type == t_cmd_start)
			{
				if (tmp->links)
					env->start = tmp;
			}
			tmp = tmp->next;
		}
	}
	return (env->start && env->end);
}

int		process_tokens_lst(t_tok **first, t_tok **last)
{
	int				path_cost;
	t_env			env;
	t_open_lst		*op_first;
	t_cls_lst		*cl_first;

	op_first = NULL;
	cl_first = NULL;
	if (check_lst_before_process(first) && check_start_end_room(last))
	{
		print_map_infos(first);
		transform_and_launch(first, &env);
		if (check_links_exists_start_and_rooms(&env))
			path_cost = launch_algo(&env, &op_first, &cl_first);
		else
			ft_error_gestion("Start or end room does not have an exit.\n");
	}
	else
		ft_error_gestion("ERROR\n");
	return (0);
}
