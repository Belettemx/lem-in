/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isvalid_room_name.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/02 17:11:24 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int				isvalid_room_name(char *str)
{
	if (ft_strlen(str) >= 1)
	{
		if (str[0] != 'L' && str[0] != '#' && !ft_strchr(str, '-'))
			return (TRUE);
	}
	return (FALSE);
}
