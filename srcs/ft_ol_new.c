/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ol_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/13 16:02:31 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdlib.h>

t_open_lst	*ft_ol_new(t_room *src, t_room *parent)
{
	t_open_lst	*elem;

	elem = (t_open_lst *)malloc(sizeof(t_open_lst));
	if (elem)
	{
		elem->elem = src;
		elem->cost = src->cost;
		elem->elem->parent = parent;
		elem->next = NULL;
	}
	return (elem);
}
