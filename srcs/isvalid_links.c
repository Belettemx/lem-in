/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isvalid_links.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/02 17:11:37 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int				isvalid_links(char **tab)
{
	if (tab && *tab && ft_lentabstr(tab) == 2)
	{
		if (isvalid_room_name(tab[0]) && isvalid_room_name(tab[1]))
			return (TRUE);
	}
	return (FALSE);
}
