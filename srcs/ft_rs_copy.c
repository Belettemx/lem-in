/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rs_copy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/15 16:29:15 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdlib.h>

t_room	*ft_rs_copy(t_room *src)
{
	t_room	*cpy;

	cpy = (t_room *)malloc(sizeof(t_room));
	if (cpy)
	{
		cpy->type = src->type;
		cpy->name = src->name;
		cpy->x = src->x;
		cpy->y = src->y;
		cpy->cost = src->cost;
		cpy->isempty = src->isempty;
		cpy->ant = src->ant;
		cpy->lock = src->lock;
		cpy->stat = src->stat;
		cpy->next = src->next;
		cpy->prev = src->prev;
		cpy->links = src->links;
		cpy->parent = src->parent;
	}
	return (cpy);
}
