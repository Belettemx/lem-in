/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_replace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/02 17:14:57 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

/*
** replace into str every character 'c' by character 'r'.
*/

int	ft_replace(char *str, char c, char r)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (str && str[i])
	{
		if (str[i] == c)
		{
			str[i] = r;
			count++;
		}
		i++;
	}
	return (count);
}
