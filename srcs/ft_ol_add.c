/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ol_add.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/15 11:21:39 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_ol_add(t_open_lst **op_first, t_open_lst *elem)
{
	t_open_lst *tmp;

	if (elem)
	{
		if (op_first && *op_first)
		{
			tmp = *op_first;
			if (tmp->cost >= elem->cost)
			{
				elem->next = tmp;
				*op_first = elem;
			}
			else
			{
				while (tmp->next != NULL && tmp->next->cost < elem->cost)
					tmp = tmp->next;
				elem->next = tmp->next;
				tmp->next = elem;
			}
		}
		else
			*op_first = elem;
	}
}
