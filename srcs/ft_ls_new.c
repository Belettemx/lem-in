/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 14:22:46 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdlib.h>

void	ft_add_elem_to_room1(t_room **room1, t_room **room2, t_links *elem)
{
	t_links	*tmp;

	elem->link = *room2;
	if ((*room1)->links == NULL)
		(*room1)->links = elem;
	else
	{
		tmp = (*room1)->links;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = elem;
	}
}

void	ft_add_elem_to_room2(t_room **room1, t_room **room2, t_links *elem)
{
	t_links	*tmp;

	elem->link = *room1;
	if ((*room2)->links == NULL)
		(*room2)->links = elem;
	else
	{
		tmp = (*room2)->links;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = elem;
	}
}

void	ft_ls_new(t_room **room1, t_room **room2)
{
	t_links	*elem1;
	t_links	*elem2;

	elem1 = (t_links *)malloc(sizeof(t_links));
	elem2 = (t_links *)malloc(sizeof(t_links));
	if (elem1 && elem2)
	{
		elem1->next = NULL;
		elem2->next = NULL;
		ft_add_elem_to_room1(room1, room2, elem1);
		ft_add_elem_to_room2(room1, room2, elem2);
	}
	else
		ft_error_gestion("ERROR\n");
}
