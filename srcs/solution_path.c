/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solution_path.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/13 16:33:44 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdlib.h>

void		reverse_path(t_cls_lst **first, t_room *start)
{
	t_room *tmp;
	t_room *parent;
	t_room *prev;

	tmp = start;
	prev = NULL;
	while (tmp)
	{
		parent = tmp->parent;
		tmp->parent = prev;
		ft_cl_add(first, ft_cl_new(tmp));
		if (parent && parent->parent == NULL)
		{
			parent->parent = tmp;
			ft_cl_add(first, ft_cl_new(parent));
			break ;
		}
		prev = tmp;
		tmp = parent;
	}
}

int			is_start_room(t_room *elem)
{
	if (elem && elem->parent == NULL)
		return (TRUE);
	return (FALSE);
}

void		print_move(t_room **cur_room)
{
	(*cur_room)->ant = (*cur_room)->parent->ant;
	(*cur_room)->isempty = FALSE;
	(*cur_room)->parent->isempty = TRUE;
	ft_putstr("L");
	ft_putnbr((*cur_room)->parent->ant);
	ft_putchar('-');
	ft_putstr((*cur_room)->name);
	ft_putchar(' ');
}

void		print_ants_travel(t_cls_lst **first, t_env *env, int current_ant)
{
	t_room	*end_room;
	t_room	*cur_room;

	end_room = (*first)->elem;
	cur_room = (*first)->elem;
	while (cur_room && end_room->ant < env->ants_nbr)
	{
		if (is_start_room(cur_room->parent) && current_ant < env->ants_nbr)
		{
			cur_room->parent->isempty = FALSE;
			current_ant++;
			cur_room->parent->ant = current_ant;
		}
		if (!cur_room->parent->isempty)
			print_move(&cur_room);
		if (is_start_room(cur_room->parent))
		{
			cur_room = (*first)->elem;
			ft_putchar('\n');
		}
		else
			cur_room = cur_room->parent;
	}
}

int			solution_path(t_cls_lst **first, t_room *elem, t_env *env)
{
	int				current_ant;
	t_cls_lst		*first_reverse;

	current_ant = 0;
	first_reverse = NULL;
	reverse_path(&first_reverse, elem);
	print_ants_travel(&first_reverse, env, current_ant);
	ft_putchar('\n');
	(void)first;
	return (0);
}
