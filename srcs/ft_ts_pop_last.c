/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ts_pop_last.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 12:45:09 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/05 17:43:42 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_tok	*ft_ts_pop_last(t_tok **first, t_tok **last)
{
	t_tok *elem;
	t_tok *cpy;

	if (first && *first && last && *last)
	{
		elem = (*last)->prev;
		elem->next = NULL;
		cpy = *last;
		last = &elem;
		return (cpy);
	}
	return (NULL);
}
