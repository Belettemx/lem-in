/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 17:28:32 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "lemin.h"

int		close_and_return(int fd, int ret)
{
	close(fd);
	return (ret);
}

void	check_invalid_type(t_tok *el, t_tok **first, t_tok **last)
{
	if (el->type != t_invalid)
	{
		if (!(el->type == t_def_room && roo_exist_update(first, el)))
			ft_ts_add_end(first, last, el);
	}
}

void	init_args(t_tok **elem, t_tok **first, t_tok **last)
{
	*elem = NULL;
	*first = NULL;
	*last = NULL;
}

int		main_loop(int fd, char *str)
{
	t_tok		*a[3];
	int			r;

	init_args(&a[2], &a[0], &a[1]);
	while (get_next_line(fd, &str) > ERROR)
	{
		if (!(a[2] = get_tokens(str)))
			return (close_and_return(fd, ERROR));
		else if (a[2]->type != t_skip)
		{
			check_invalid_type(a[2], &a[0], &a[1]);
			if ((r = check_lst_tokens(&a[0])) == ERROR)
				return (close_and_return(fd, ERROR));
			else if ((r == FALSE || (a[2] && a[2]->type == t_invalid)) && a[0])
			{
				if (a[2] && a[2]->type != t_invalid)
					free((void*)ft_ts_pop_last(&a[0], &a[1]));
				break ;
			}
			else if (r == FALSE || (a[2] && a[2]->type == t_invalid))
				return (close_and_return(fd, ERROR));
		}
	}
	close(fd);
	return (process_tokens_lst(&a[0], &a[1]));
}

int		main(int ac, char **av)
{
	int			fd;
	char		*str;

	fd = 0;
	(void)av;
	str = NULL;
	if (ac == 1)
	{
		if (main_loop(fd, str) == ERROR)
			ft_error_gestion("ERROR\n");
	}
	else
		ft_putstr("ERROR\n");
	return (0);
}
