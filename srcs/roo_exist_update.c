/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   roo_exist_update.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 16:06:24 by agauci-d          #+#    #+#             */
/*   Updated: 2016/12/09 17:50:36 by agauci-d         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lemin.h"

int		same_name(t_tok *elem, t_tok *cur)
{
	if (ft_strequ(elem->str, cur->str))
		return (TRUE);
	return (FALSE);
}

int		same_coord(t_tok *elem, t_tok *cur)
{
	if (ft_strequ(elem->x, cur->x) && ft_strequ(elem->y, cur->y))
		return (TRUE);
	return (FALSE);
}

int		roo_exist_update(t_tok **first, t_tok *elem)
{
	t_tok	*tmp;
	int		updated;

	updated = FALSE;
	if (first && *first)
	{
		tmp = *first;
		while (tmp)
		{
			if (tmp->type == t_def_room && same_name(elem, tmp))
			{
				tmp->x = elem->x;
				tmp->y = elem->y;
				updated = TRUE;
			}
			else if (tmp->type == t_def_room && same_coord(elem, tmp))
			{
				tmp->str = elem->str;
				updated = TRUE;
			}
			tmp = tmp->next;
		}
	}
	return (updated);
}
